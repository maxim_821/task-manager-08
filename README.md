# TASK MANAGER

Console application for task list.

# DEVELOPER INFO

NAME: Max Ivanov

# SOFTWARE

* JDK 1.8

* Windows

# HARDWARE

* RAM 12Gb

* CPU i5

* HDD 128Gb

# BUILD PROGRAM 

```
mvn clean install 
```

# RUN PROGRAM

```
java -jar ./task-manager.jar
```
